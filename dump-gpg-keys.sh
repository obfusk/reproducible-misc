#!/bin/bash

# requires: GPG 2.1

# dumps out our minimal keys, useful to create nicely formatted
# debian/upstream/signin-keys.asc in our packages


set -eu -o pipefail

removetemp () { rm -rf "$gpghome" && echo "$gpghome removed" >&2 ; }
gpghome="$(mktemp -d)"
trap removetemp EXIT

GPG=("gpg" "--homedir" "$gpghome" "--batch")

if [ $# -gt 0 ]; then
    keys=("$@")
else
    keys=(
        C2FE4BD271C139B86C533E461E953E27D4311E58    # lamby@debian.org
        66AE2B4AFCCF3F52DA184D184B043FCDB9444540    # mattia@debian.org
    )
fi

# import all the keys
# run over multiple keyservers, so to make sure to pick up the latests updated
keyservers=(
    keyring.debian.org
    keyserver.ubuntu.com
    keys.openpgp.org
)

for key in "${keys[@]}"; do
    if [ -f "$key" ]; then
        action="--import"
        keyIDs+=("$("${GPG[@]}" --with-colons "$key" | cut -d: -f5)")
    else
        action="--recv-keys"
        keyIDs+=("$key")
    fi
    for _k in "${keyservers[@]}"; do
        "${GPG[@]}" \
            --keyserver "${_k}" \
            "$action" \
            "$key" || echo "W: $_k returned no useful data for $key, continuing…" >&2
    done
done


if [ -z "$("${GPG[@]}" -k 2>/dev/null)" ]; then
    echo "E: None of the wanted keys was found in any keyserver." >&2
    exit 1
fi

# print some information about the keys
# (--fingerprint twice so the fingeprint of the subkeys is printed too)
"${GPG[@]}" \
    --list-keys \
    --keyid-format none \
    --with-subkey-fingerprint \
    --list-options no-show-keyring \
    | tail -n +3

# re-export them
"${GPG[@]}" \
    --export \
    --armor \
    --export-options export-clean,export-minimal \
    "${keyIDs[@]}"
