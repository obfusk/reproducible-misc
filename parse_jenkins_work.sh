#!/bin/bash
# Copyright 2017 Holger Levsen <holger@layer-acht.org>
# released under the GPLv=2

#
# FIXME: this really needs more polishing....
#
#


BEGIN=2017-11-19
END=2017-11-25
WORK=$(mktemp)
IFS=$'\n'

echo "Report for week between $BEGIN and $END."
echo
echo
echo tests.reproducible-builds.org
echo ===============================================================
echo

git log --reverse --grep reproducible -i --since $BEGIN --until $END > $WORK
# $WORKERS are authors and signers
WORKERS=$( ( grep Author $WORK | cut -d " " -f2- ; grep Signed-off-by $WORK | cut -d ':' -f2-| sort -u | sed -s "s#^ ##" ) | sort -u ) 
for author in $WORKERS ; do
	shortauthor=$(echo $author | cut -d ' ' -f1)
	echo "* $author" | cut -d '<' -f1
	for commit in $(git log --reverse --oneline --grep reproducible -i --author "$author" --since $BEGIN --until $END |cut -d ' ' -f1) ; do
		echo -n "  * [$commit](https://anonscm.debian.org/git/qa/jenkins.debian.net.git/commit/?id=$commit) - "
		git log $commit -1 | egrep -v '^(Date|Author|commit|    Signed-off-by|    $|$)'
	done
	
	COUNT=0
	for j in $WORKERS ; do
		if [ "$j" = "$author" ] ; then
			continue
		fi
		MORE=$(git log --reverse --grep reproducible -i --author "$j" --since $BEGIN --until $END | grep -c "Signed-off-by: $author")
		let COUNT+=$MORE || true
	done
	if [ $COUNT -gt 0 ] ; then
		echo  "  * $shortauthor also reviewed and deployed $COUNT commits from other people."
	fi
	echo 
done
echo

rm $WORK
