#!/bin/sh
set -eu
# TODO: regex escaping
pkg=$1

git log --format=%H -G '^'${pkg}':' | while read x; do
    git log -n1 $x
    git show $x:packages.yml | sed -n '/^'${pkg}':/,/^[^ ]/p'
done

