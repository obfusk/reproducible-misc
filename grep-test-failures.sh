#!/bin/sh

(
    cd rbuilds
    grep -l 't/.*Failed: [^0])' *
    grep -l 'ERROR: Test "ruby..." failed. Exiting.' *
    fgrep 'FAILED (errors=' *
    fgrep 'FAILED (failures=' *
    fgrep -l "recipe for target 'check' failed" *
    grep 'FAIL:  [^0]' *
) | cut -d_ -f1
