#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging


def setup_logging(name, verbose=False):
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG if verbose else logging.INFO)
    sh = logging.StreamHandler()
    sh.setFormatter(logging.Formatter('{levelname[0]}: {message}', style='{'))
    log.addHandler(sh)

    return log
