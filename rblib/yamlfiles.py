#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import yaml

from . import logger

NOTES_YAML = 'packages.yml'

log = logger.setup_logging(__name__)

def load_notes():
    try:
        with open(NOTES_YAML, encoding='utf-8') as f:
            return yaml.safe_load(f)
    except FileNotFoundError:

        log.error('%s has not been found in your current directory.  Please '
                  'cd to the notes directory', NOTES_YAML)
        sys.exit(1)


def write_out(notes):
    out = ''
    for pkg, values in sorted(notes.items(), key=str):
        if not any(values.get(key, None)
                   for key in ['comments', 'issues', 'bugs']):
            continue

        if pkg[0:2] == '0x':  # otherwise something converts 0xffff to 65535
            out += "!!str %s:\n" % pkg
        else:
            out += "%s:\n" % pkg
        try:
            ver = values['version']
            if isinstance(ver, str):
                try:
                    if float(ver) != ver:
                        out += "  version: '%s'\n" % ver
                except ValueError:
                    out += "  version: %s\n" % ver
            else:
                out += "  version: %s\n" % ver
        except KeyError:
            pass
        if 'comments' in values:
            out += "  comments: |\n"
            for line in values['comments'].strip().split('\n'):
                out += "    %s\n" % line
        if 'issues' in values and values['issues']:
            out += "  issues:\n"
            for issue in values['issues']:
                out += "    - %s\n" % issue
        if 'bugs' in values and values['bugs']:
            out += "  bugs:\n"
            for bug in sorted(set(values['bugs'])):
                # this check is run here since I'm not really sure where else
                # to dump it, since I couldn't find any better place.
                try:
                    assert isinstance(bug, int)
                except AssertionError:
                    log.critical('The note for %s contains an invalid bug '
                                 'number that is not a number.  Fix manually.',
                                 pkg)
                    raise
                out += "    - %s\n" % bug
    with open(NOTES_YAML, 'wb') as f:
        f.write(out.encode('utf-8'))
