#!/usr/bin/python3
# -*- coding: utf-8 -*-

import bz2
import json
import os
import tempfile
import time
import sys

import psycopg2
import requests

from . import logger

RB_SITE = 'https://tests.reproducible-builds.org'
REPRODUCIBLE_JSON = '{}/debian/reproducible.json.bz2'.format(RB_SITE)
JSON_CACHE_FILE = 'reproducible.json'
# [{package: xxx, suite: sid, version: 0.0.0, status: reproducible}, {...}]

log = logger.setup_logging(__name__)


def load_json_cache():
    with open(JSON_CACHE_FILE) as f:
        return json.load(f)


def load_json():
    CACHE_SECONDS = 10 * 60
    try:
        mtime = os.path.getmtime(JSON_CACHE_FILE)
    except FileNotFoundError:
        mtime = None

    if mtime and time.time() - mtime < CACHE_SECONDS:
        return load_json_cache()

    try:
        content = bz2.decompress(requests.get(REPRODUCIBLE_JSON).content)
        fd, temp_name = tempfile.mkstemp(prefix='.', suffix='.json', dir='.')
        with open(fd, 'wb') as f:
            f.write(content)
        os.rename(temp_name, JSON_CACHE_FILE)
        os.chmod(JSON_CACHE_FILE, 0o644)
    except Exception as e:
        sys.stderr.write('downloading cache file failed ({}), trying cached...\n'.format(e))

    return load_json_cache()


def load_reproducible_status():
    status = {}
    rstatus = load_json()

    for item in rstatus:
        pkg = item['package']
        version = str(item['version'])

        # :(
        if item['architecture'] != 'amd64':
            continue

        # feed status with the unstable value, if available, otherwise prefer
        # testing and then experimental
        try:
            if status[pkg]['suite'] == 'unstable':
                continue  # that's the best
            elif status[pkg]['suite'] == 'experimental':  # whatever we've now
                status[pkg]['version'] = version          # is better
                status[pkg]['suite'] = item['suite']
                status[pkg]['status'] = item['status']
            elif status[pkg]['suite'] != 'unstable' and \
                    item['suite'] != 'experimental':
                status[pkg]['version'] = version
                status[pkg]['suite'] = item['suite']
                status[pkg]['status'] = item['status']
        except KeyError:
            status[pkg] = {
                'version': version,
                'suite': item['suite'],
                'status': item['status'],
            }

    return status


def start_udd_connection():
    username = "udd-mirror"
    password = "udd-mirror"
    host = "udd-mirror.debian.net"
    port = 5432
    db = "udd"
    try:
        try:
            log.debug("Starting connection to the UDD database")
            conn = psycopg2.connect(
                database=db,
                user=username,
                host=host,
                port=port,
                password=password,
                connect_timeout=5,
            )
        except psycopg2.OperationalError as err:
            if str(err) == 'timeout expired\n':
                log.error('Connection to the UDD database replice timed out. '
                          'Maybe the machine is offline or just unavailable.')
                log.error('Failing nicely anyway, all queries will return an '
                          'empty response.')
                return None
            else:
                raise
    except Exception:
        log.exception('Erorr connecting to the UDD database replica.' +
                      'The full error is:')
        log.error('Failing nicely anyway, all queries will return an empty ' +
                  'response.')
        return None
    conn.set_client_encoding('utf8')
    return conn


def query_udd(conn_udd, query):
    if not conn_udd:
        log.error('There has been an error connecting to the UDD database. ' +
                  'Please look for a previous error for more information.')
        log.error('Failing nicely anyway, returning an empty response.')
        return []
    cursor = conn_udd.cursor()
    try:
        cursor.execute(query)
    except Exception:
        log.exception('The UDD server encountered a issue while executing the'
                      ' query. The full error is:')
        log.error('Failing nicely anyway, returning an empty response.')
        return []
    return cursor.fetchall()
