#!/usr/bin/perl
#
# Print packages from the notes with the date
# of their last modification.
# Can be used to find oldest notes, e.g.
# list-notes-age.pl | sort -r
#

use strict;
use warnings;

use POSIX qw(strftime);

my $file = "packages.yml";
my $packages = {};
my $pkg;

die "Can't find $file." unless(-e $file);

open FH, "git blame -t $file|" or die "$!";
while(<FH>) {
    if (m/[\da-f]+ .*\s+(\d{10}) [\d\-\+]+\s+\d+\) (.*)/) {
        my $timestamp = $1;
        my $line = $2;
        if ($line =~ m/^([^ \t].*):/) {
            $pkg = $1;
        }

        if (!exists($packages->{$pkg}) or $packages->{$pkg} < $timestamp) {
            $packages->{$pkg} = $timestamp;
        }
    }
}
close FH;

foreach (sort keys %$packages) {
    my $date = strftime("%F", gmtime($packages->{$_}));
    print "$date  $_\n";
}
